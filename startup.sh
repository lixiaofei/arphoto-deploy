#!/bin/sh

# -----------------------------------------------------------------------------
# Start Script for the CATALINA Server
#
# author zw Create on 2012-01-5 ÏÂÎç06:28:00
# -----------------------------------------------------------------------------

# java home
JAVA_HOME=/usr/java/jdk1.8.0_131/
# game home
GAME_HOME=${GAME_HOME:-/home/lixiaofei/arphoto-deploy}
# pid file
PID_FILE=${GAME_HOME}/pid_file
# java bin path
JAVA_PATH=${JAVA_HOME}/bin/java
#
ServerName=ArPhoto1001
# Main java
MainJava=com.lee.arphoto.App
# Main jar
MainJar=arphoto-0.0.1-SNAPSHOT.jar

if
        [ -z "`echo $PATH | grep $JAVAPATH`" ];
then
        export PATH=${PATH}:${JAVA_PATH}
fi

# enter game home
cd ${GAME_HOME}
# load jar file
LIB_CP=$(echo `ls lib/*.jar` | sed 's/\s/:/g')

GAME_CP="./config-server/${MainJar}:${LIB_CP}:./config-server/"

echo "=================================================================================="
# echo "classpath: ${GAME_CP}"
echo "Will startup!"
echo "=================================================================================="


MEM_PARAM="-Xms256m -Xmx256m -Xmn100m -Xss288k -XX:SurvivorRatio=1 -XX:TargetSurvivorRatio=80 -XX:PermSize=64m -XX:MaxPermSize=64m -XX:MaxTenuringThreshold=3"

GC_PARAM="-XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:ParallelGCThreads=4 -XX:+UseCMSCompactAtFullCollection -XX:+CMSScavengeBeforeRemark -XX:CMSFullGCsBeforeCompaction=1 -XX:CMSInitiatingOccupancyFraction=60"

TER_PARAM="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${GAME_HOME}/logs_gc/ -XX:+PrintGCDateStamps -XX:+PrintGCDetails -Xloggc:${GAME_HOME}/logs_gc/gc.log"

# start  server
#${JAVA_PATH} -server ${MEM_PARAM} ${GC_PARAM} ${TER_PARAM} -cp ${GAME_CP} ${MainJava} -Dmyapp.name=${ServerName} >std.log 2>&1 &
${JAVA_PATH} -server -cp ${GAME_CP} ${MainJava} -Dmyapp.name=${ServerName} >std.log 2>&1 &



# delete pid file
rm -f ${GAME_HOME}/${PID_FILE}

# find pid of including 'arphoto.App' 'java' info, after that write pid file
ps ax|grep ${MainJava} |grep 'java' |grep ${ServerName} |awk '{print $1}' > ${PID_FILE}

# listener std.log 
tail -f std.log 

#echo "please wait ..." 
#sleep 6
