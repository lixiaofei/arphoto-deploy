/*
SQLyog v10.2 
MySQL - 5.6.5-m8 : Database - arphoto
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `albumunit` */

CREATE TABLE `albumunit` (
  `id` bigint(200) DEFAULT NULL,
  `picId` bigint(200) DEFAULT NULL COMMENT '照片',
  `videoId` varchar(200) DEFAULT NULL COMMENT '视频id，例如:1,2,3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `c3p0testtable` */

CREATE TABLE `c3p0testtable` (
  `a` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `pic` */

CREATE TABLE `pic` (
  `id` bigint(200) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  `downloadPath` varchar(500) DEFAULT NULL COMMENT '下载地址',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

CREATE TABLE `user` (
  `id` bigint(64) DEFAULT NULL COMMENT '唯一id',
  `userName` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `nikeName` varchar(50) DEFAULT NULL COMMENT '昵称',
  `sex` bit(1) DEFAULT NULL COMMENT '性别:0女,1男',
  `homePicUrl` varchar(500) DEFAULT NULL COMMENT '主页背景图',
  `platformId` int(10) DEFAULT NULL COMMENT '平台id',
  `platformName` varchar(10) DEFAULT NULL COMMENT '平台名称，小米，腾讯',
  `createTime` datetime DEFAULT NULL,
  `lastLoginTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `useralbum` */

CREATE TABLE `useralbum` (
  `id` bigint(200) DEFAULT NULL,
  `userId` bigint(200) DEFAULT NULL,
  `data` text COMMENT '数据 : 例如相册数据的 id1,id2,id3',
  `friendsData` text COMMENT '好友分享过来的相册数据 : 例如相册数据的 id1,id2,id3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `video` */

CREATE TABLE `video` (
  `id` bigint(200) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  `downloadPath` varchar(500) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=dec8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
